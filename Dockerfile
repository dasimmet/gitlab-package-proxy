FROM docker.io/python:latest

ADD . /tmp/src

RUN pip install --no-cache-dir /tmp/src && \
    rm -rf /tmp/src && \
    gitlabproxy --help