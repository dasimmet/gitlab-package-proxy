from socketserver import ForkingTCPServer

from .gitlab import GLServer
from .gitlab import Group
from .gitlab import RESTObject
from .gitlab import Project

ForkingTCPServer.allow_reuse_address = True

class ProxyServer(ForkingTCPServer):
    gitlab: GLServer
    default_object: RESTObject
    def __init__(self, *args, token: str=None, url: str=None, **kwargs):
        from urllib.parse import urlparse
        
        u = urlparse(url)
        if not u.netloc:
            u = urlparse("https://" + url)

        self.gitlab = GLServer(u.geturl(), token)
        
        
        if u.path.strip("/"):
            self.default_object = self.gitlab.group.get_path(u.path.strip("/"))
            if not self.default_object:
                self.default_object = self.gitlab.project.get_path(self.gitlab, u.path.strip("/"))
                print(self.default_object)
        
        
        ForkingTCPServer.__init__(self, *args, **kwargs)
        self.allow_reuse_address = True

