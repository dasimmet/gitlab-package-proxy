

from .handler import Handler
import argparse
from .proxyserver import ProxyServer

def main():
    import os
    p = parser()
    args = p.parse_args()
    
    if not args.token:
        if "GL_TOKEN" in os.environ:
            args.token = os.getenv("GL_TOKEN")
        elif "CI_JOB_TOKEN" in os.environ:
            args.token = os.getenv("CI_JOB_TOKEN")

    httpd =  ProxyServer(('', args.port), Handler, token=args.token, url=args.url)

    URL = "http://localhost:{}".format(args.port)
    print ("Now serving at ", URL)
    httpd.serve_forever()

def parser():
    p = argparse.ArgumentParser()
    p.add_argument('-t', '--token', default='', help='URL to gitlab user, project or group')
    p.add_argument('-p', '--port', default=9097, type=int, help='port to listen on')
    p.add_argument('url', help='URL to gitlab user, project or group')
    return p
