import sys
class HTMLOutput():
    header: str = """
<!DOCTYPE HTML PUBLIC "-//W3C/DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset={enc}">
<title>{title}</title>
</head>
<body>
<h1>{title}</h1>
<p><a href="..">..</a></p>
"""
    footer: str = """
</body>
</html>
"""
    linkentry: str = '<li><a href="{}">{}</a></li>'

    @classmethod
    def listdir(cls, items):
        enc = sys.getdefaultencoding()
        yield cls.header.format(title="Directory Listing", enc=enc)
        yield "<ul>"
        
        for it in items:            
            yield cls.linkentry.format(it['url'], it['name'])
    
        yield "</ul>"
        yield cls.footer

    @classmethod
    def format_output(cls, response):
        enc = sys.getdefaultencoding()
        yield cls.header.format(title="Object Listing", enc=enc)
        yield '<div>'
        from collections.abc import Generator, Iterator
        iter_class = False
        for cls in [list, Generator, Iterator]:
            if isinstance(response,cls):
                iter_class = True
                for it in response:
                    yield it.to_html()
                    
        if not iter_class:
            yield response.to_html()
        yield '</div>'