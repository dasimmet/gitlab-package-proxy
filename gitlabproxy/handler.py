
from http.server import SimpleHTTPRequestHandler
from urllib.parse import urlparse,ParseResult,parse_qs
from .proxyserver import ProxyServer
from .HTMLOutput import HTMLOutput
from .gitlab.Package import Package

class Handler(SimpleHTTPRequestHandler):
    server: ProxyServer
    requrl: ParseResult
    query: dict
    path_split: list
    content_type: str = 'text/html'
    formatter = HTMLOutput.format_output

    def do_GET(self):
        addr = self.server.server_address
        self.requrl = urlparse("http://{}:{}{}".format(addr[0],addr[1],self.path))
        self.query = parse_qs(self.requrl.query, keep_blank_values=True)
        if 'json' in self.query:
            self.content_type = 'application/json'
        if 'raw' in self.query:
            self.content_type = 'application/octet-stream'
        print(self.requrl)
        print(self.query)

        self.path_split = [s.lower() for s in self.requrl.path.split("/")[1:]]
        
        if len(self.path_split) > 0 and self.path_split[0] != "":
            if self.path_split[0] == "packages":
                response = self.server.default_object.package.list(query=self.query)
            elif self.path_split[0] == "packagefiles":
                import itertools
                response = []
                for pkg in self.server.default_object.package.list(query=self.query):
                    pkg: Package = pkg
                    response = itertools.chain(response, pkg.files.list())
                    # response = response + list(pkg.files.list())
            elif self.path_split[0] in self.server.default_object.json:
                response = self.server.default_object.json[self.path_split[0]]
            else:
                self.send_response(307)
                self.send_header("Location", "/")
                self.end_headers()
                return
        else:
            response = self.server.default_object
        try:
            self.format_output(response)
        except Exception as ex:
            import traceback
            print(traceback.format_exc())
            self.wfile.write(('<raw>'+traceback.format_exc()+'</raw>').encode())
        
    def format_output(self, response):
        self.send_response(200)
        self.send_header("Content-Type", self.content_type)
        self.end_headers()
        if self.content_type == 'application/octet-stream':
            self.copyfile(response, self.wfile)
        elif self.content_type == 'text/html':
            for l in self.formatter(response):
                self.wfile.write(l.encode())
        elif self.content_type == 'application/json':
            if type(response) in [dict,list,str,int]:
                import json
                self.wfile.write(json.dumps(response,indent=4).encode())
                return
            self.wfile.write(str(response.dumps(indent=4)).encode())
            return