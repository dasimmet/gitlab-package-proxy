from .RESTEndpoint import RESTEndpoint
from .RESTObject import RESTObject
from .Package import Package
class Project(RESTObject):

    endpoints: RESTEndpoint = RESTEndpoint(
        get = "/api/v4/projects/{id}",
        search = "/api/v4/projects"
    )

    package: Package = RESTObject.child(Package)