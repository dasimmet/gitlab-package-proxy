from .RESTEndpoint import RESTEndpoint
from .RESTObject import RESTObject
from .Package import Package

class Group(RESTObject):

    endpoints:RESTEndpoint = RESTEndpoint(
        get = "/api/v4/groups/{id}"
    )
    
    package: Package = RESTObject.child(Package)