from urllib.parse import urlparse,ParseResult

class GLServer():
    url: ParseResult
    token: str
    
    def __init__(self, url: str, token: str):
        self.url = urlparse(url)._replace(path='')
        self.token = token
        from .Group import Group
        from .Project import Project
        self.project: Project = Project(self)
        self.group: Group = Group(self)

    @property
    def request(self):
        from .GLRequest import GLRequest
        return GLRequest(self)