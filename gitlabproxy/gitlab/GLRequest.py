
import json
from urllib.parse import urlencode
from urllib.request import Request,urlopen
from urllib.error import HTTPError
from .GLServer import GLServer

class GLRequest():
    instance: GLServer
    headers: dict
    def __init__(self, instance: GLServer):
        self.instance = instance
        self.headers = {
            "PRIVATE-TOKEN": self.instance.token,
        }
    
    def ep_url(self, ep, query={}) -> str:
        q = self.instance.url._replace(path=ep, query=urlencode(query, doseq=True))
        print("REQURL:", q)
        return q.geturl()

    def put_file(self, url, filepath, method="PUT"):
        import os
        stat = os.stat(filepath)
        self.headers["Content-Length"] = stat.st_size
        self.headers["Accept"] = "*/*"

        with open(filepath,'rb') as fd:
            req = Request(url, data=fd, headers=self.headers, method=method)
            try:
                with urlopen(req) as res:
                    return json.loads(res.read())
            except HTTPError as e:
                print(e.read().decode())
                if e.code == 400:
                    import sys
                    sys.exit(1)
                raise e


    def get_raw(self, endpoint, method="GET"):
        requrl = self.ep_url(endpoint)
        req = Request(requrl, headers=self.headers, method=method)
        return urlopen(req)

    def get(self, endpoint, method="GET", query={}):
        requrl = self.ep_url(endpoint, query=query)
        req = Request(requrl, headers=self.headers, method=method)
        try:
            with urlopen(req) as res:
                return json.loads(res.read())
        except HTTPError as e:
            return json.loads(e.read())


    def get_paged(self, endpoint, method="GET", query={}):
        print("QUERY:", query)
        requrl = self.ep_url(endpoint, query=query)
        req = Request(requrl, headers=self.headers, method=method)
        next_url = None
        try:
            with urlopen(req) as res:
                if 'X-Total-Pages' in res.headers:
                    if res.headers['X-Page'] < res.headers['X-Total-Pages']:
                        from .link_header import parse_link_value
                        for key,value in parse_link_value(res.headers['Link']).items():
                            if key == 'next':
                                next_url = value['url']
                
                for r in json.loads(res.read()):
                    jprint(r)
                    yield r
        except HTTPError as e:
            if e.code == 308:
                return
            raise e

        if next_url:
            print("NEXTURL:", next_url)
            for res in self.get_paged(next_url, method):
                yield res


    def post(self, url, payload, method="POST"):
        self.headers["Content-Type"] = "application/json"
        req = Request(url, json.dumps(payload).encode(),headers=self.headers, method=method)
        with urlopen(req) as res:
            jres = json.loads(res.read())
        return jres


def jprint(j,*args,**kwargs):
    print(json.dumps(j, indent=4), *args, **kwargs)


