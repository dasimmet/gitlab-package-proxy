from .RESTEndpoint import RESTEndpoint
from .RESTObject import RESTObject

class PackageFile(RESTObject):
    
    needs_parent_attr = {
        'project_id': 'project_id',
        'package_id': 'id',
        'package_name': 'name',
        'package_version': 'version',
        'project_path': 'project_path',
    }
    endpoints:RESTEndpoint = RESTEndpoint(
        list_from_Package = "/api/v4/projects/{project_id}/packages/{package_id}/package_files"
    )
