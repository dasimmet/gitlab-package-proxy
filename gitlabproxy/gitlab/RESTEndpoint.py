
class QuoteFormatString(str):
    
    def format_quote(self, *args, **kwargs):
        from urllib.parse import quote_plus
        return self.format(
            *[quote_plus(str(arg)) for arg in args],
            **{k:quote_plus(str(v)) for k,v in kwargs.items()}
        )
class RESTEndpoint():
    def __init__(self, **kwargs):
        for arg,value in kwargs.items():
            setattr(self, arg, QuoteFormatString(value))
