from .RESTEndpoint import RESTEndpoint
from .GLServer import GLServer
import json
class RESTOptions():
    instance: GLServer
class RESTObject():
    _opt: RESTOptions = None
    needs_parent_attr: dict[str,str] = {}
    json: dict
    endpoints: RESTEndpoint
    parent = None
    
    
    @property
    def parent_attr(self) -> dict:
        attr = {}
        if self.parent:
            for k,v in self.needs_parent_attr.items():
                attr[k] = self.parent.json[v]
        return attr

    @staticmethod
    def child(childClass):
        def instanciate(self, cls=childClass) -> childClass:
            return childClass(parent=self)
        return property(instanciate)
        
    @property
    def opt(self) -> RESTOptions:
        if not self._opt:
            return self.parent.opt
        return self._opt

    def __init__(self, server: GLServer = None, json: dict={}, parent=None):
        self.json = json
        if server:
            self._opt = RESTOptions()
            self._opt.instance = server
        self.parent = parent
        if parent:
            self.endpoints.list = getattr(self.endpoints, 'list_from_' + parent.__class__.__name__)
            try:
                self.endpoints.get = getattr(self.endpoints, 'get_from_' + parent.__class__.__name__)
            except:
                pass
    
    def get_path(self, path: str):
        attrs = self.parent_attr
        attrs['id'] = path
        object_url = self.endpoints.get.format_quote(**attrs)
        obj = self.__class__(self.opt.instance, self.opt.instance.request.get(object_url))
        if not 'id' in obj.json:
            return None
        return obj

    def get_id(self, id: int):
        attrs = self.parent_attr
        attrs['id'] = id
        object_url = self.endpoints.get.format(**attrs)
        obj = self.__class__(self.opt.instance, self.opt.instance.request.get(object_url))
        if not 'id' in obj.json:
            return None
        return obj

    def list(self, query: dict = {}) -> object:
        attrs = self.parent_attr
        object_url = self.endpoints.list.format(**attrs)
        for o in self.opt.instance.request.get_paged(object_url, query=query):
            yield self.__class__(self.opt.instance, o, parent=self.parent)

    def dumps(self,*args,**kwargs) -> str:
        return json.dumps(self.json, *args,**kwargs)
                
    def to_html(self) -> str:
        html = ""
        hjson = self.json.copy()
        if 'name' in self.json:
            html += "<h1>{}</h1><p>".format(self.json['name'])
        
        for k in hjson:
            v = hjson[k]
            if k.rfind('url') >= 0:
                hjson[k] = '<a href="{}">{}</a>'.format(v,v)
            if k.rfind('path') >= 0:
                url = self.opt.instance.url._replace(path=v)
                hjson[k] = '<a href={}>{}</a>'.format(url.geturl(),v)
        if len(self.parent_attr) > 0:
            html += '<h3>Parent Attr:</h3><pre><code>{}</code></pre>'.format(json.dumps(self.parent_attr, indent=4))
        html += '<h3>JSON:</h3><pre><code>{}</code></pre></p>'.format(json.dumps(hjson, indent=4))
        return html


    def __repr__(self) -> str:
        return "{}(server={},json={},parent={})".format(self.__class__.__name__, self.opt.instance,json.dumps(self.json),self.parent)