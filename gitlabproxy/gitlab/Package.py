from .RESTEndpoint import RESTEndpoint
from .RESTObject import RESTObject
from .PackageFile import PackageFile

class Package(RESTObject):

    needs_parent_attr = {
        'parent_id': 'id'
    }
    endpoints:RESTEndpoint = RESTEndpoint(
        get_from_Project = "/api/v4/projects/{parent_id}/packages/{id}",
        get_from_Group = "/api/v4/groups/{parent_id}/packages/{id}",
        list_from_Project = "/api/v4/projects/{parent_id}/packages",
        list_from_Group = "/api/v4/groups/{parent_id}/packages",
    )
    
    files: PackageFile = RESTObject.child(PackageFile)
